package StepDefinitions;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;

public class Steps {

    WebDriver browser;

    @Given("Un usuario que quiere comprar una consola de video juego ingresa a la página {string}")
    public void un_usuario_que_quiere_comprar_una_consola_de_video_juego_ingresa_a_la_página(String URL) {
        System.setProperty("webdriver.geckodriver.browser", "/Users/macbook/Desktop/GitRepoFull/Prueba_Falabella/driver/geckodriver");
        browser = new FirefoxDriver();
        browser.manage().window().maximize();
        browser.get(URL);
    }

    @Given("entonces espera que la página se cargue por completo")
    public void entonces_espera_que_la_página_se_cargue_por_completo() {
        new WebDriverWait(browser, 10).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    @Given("Luego se dirige a {string} luego a {string}")
    public void luego_se_dirige_a_luego_a(String categoria, String tegnologia) {
        String IDCat = "hamburgerMenu";
        String IDTec = "item-3";
        if(browser.findElement(By.id(IDCat)).isDisplayed())
        {
            browser.findElement(By.id(IDCat)).click();
            browser.findElement(By.id(IDTec)).click();
        }
    }

    @Given("para hacer clic en {string} en la sección de Videojuegos")
    public void para_hacer_clic_en_en_la_sección_de_Videojuegos(String consolas) {
        String btnconsolaCSS = ".SecondLevelItems_displaySubMenuDesktop__33Gpt > div:nth-child(1) > div:nth-child(1) > ul:nth-child(5) > div:nth-child(2) > li:nth-child(3) > a:nth-child(1)";
        if(browser.findElement(By.cssSelector(btnconsolaCSS)).isDisplayed())
        {
            browser.findElement(By.cssSelector(btnconsolaCSS)).click();
        }
    }

    @Given("entonces puede {string} por marca {string}")
    public void entonces_puede_por(String filtro, String marca) {
        String IDfiltro = "testId-Accordion-Marca";
        String marcaCSS = "li.jsx-53718149:nth-child(9) > label:nth-child(1) > span:nth-child(2) > span:nth-child(1)";

        if(browser.findElement(By.id(IDfiltro)).isDisplayed())
        {
            browser.findElement(By.id(IDfiltro)).click();
            if(browser.findElement(By.cssSelector(marcaCSS)).isDisplayed())
            {
                browser.findElement(By.cssSelector(marcaCSS)).click();
            }
        }
    }
    @Given("luego selecciona el botón {string}")
    public void luego_selecciona_el_botón(String verprod) throws InterruptedException {
        String btnVerProducto = "testId-Pod-action-8014967";
        Thread.sleep(3000);
        if(browser.findElement(By.id(btnVerProducto)).isDisplayed())
        {
            browser.findElement(By.id(btnVerProducto)).click();
        }
    }

    @Given("agrega el producto a la {string}")
    public void agrega_el_producto_a_la(String string) {
        String IDbtnAgregar = "buttonForCustomers";
        if(browser.findElement(By.id(IDbtnAgregar)).isDisplayed())
        {
            browser.findElement(By.id(IDbtnAgregar)).click();
        }
    }

    @Given("y selecciona el botón {string}")
    public void y_selecciona_el_botón(String string) {
        String IDbtnAgregar2 = "linkButton";
        if(browser.findElement(By.id(IDbtnAgregar2)).isDisplayed())
        {
            browser.findElement(By.id(IDbtnAgregar2)).click();
        }
    }

    @Given("estando en la bolsa, aumentar a {string} la cantidad de productos")
    public void estando_en_la_bolsa_aumentar_a_la_cantidad_de_productos(String string) throws InterruptedException {
        String CSSmas = ".increase";
        if(browser.findElement(By.cssSelector(CSSmas)).isDisplayed())
        {
            browser.findElement(By.cssSelector(CSSmas)).click();
            Thread.sleep(3000);
            browser.findElement(By.cssSelector(CSSmas)).click();
        }

    }

    @Given("para finalizar, {string} el navegador")
    public void para_finalizar_el_navegador(String string)throws InterruptedException {
        Thread.sleep(5000);
        browser.quit();
    }

}
