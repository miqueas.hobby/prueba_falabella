Feature: Compra no finalizada en Falabella.com

  Scenario: Flujo compra en Falabella.com
    * Un usuario que quiere comprar una consola de video juego ingresa a la página "https://www.falabella.com/falabella-cl/"
    * entonces espera que la página se cargue por completo
    * Luego se dirige a "CATEGORIAS" luego a "TECNOLOGIA"
    * para hacer clic en "CONSOLAS" en la sección de Videojuegos
    * entonces puede "FILTRAR" por marca "Nintendo"
    * luego selecciona el botón "VER PRODUCTO"
    * agrega el producto a la "BOLSA DE COMPRAS"
    * y selecciona el botón "VER BOLSA DE COMPRAS"
    * estando en la bolsa, aumentar a "3" la cantidad de productos
    * para finalizar, "cerrar" el navegador

